"""Mine the NAS."""

__version__ = "2024.03.13"

__all__ = [
    "dicom",
    "edf",
    "ixtrend",
    "mict",
    "syringes",
]

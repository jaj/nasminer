import hashlib
import logging
from functools import partial
from os import getenv
from pathlib import Path
from re import match

import coloredlogs

config = {
    "dburi": getenv("NASMINER_DBURI"),
    "dbschema": getenv("NASMINER_DBSCHEMA", "public"),
    "deletemaxrows": getenv("NASMINER_DELETEMAXROWS"),
    "datadir": getenv("NASMINER_DATADIR", "/data"),
    "outputdir": getenv("NASMINER_OUTPUTDIR", "/output"),
    "debug": bool(getenv("NASMINER_DEBUG", "True")),
    "usetestbase": False,
}


logger = logging.getLogger("nasminer")
# logger.setLevel(logging.DEBUG)
# ch = logging.StreamHandler()
# ch.setLevel(logging.DEBUG)
# formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# ch.setFormatter(formatter)
# logger.addHandler(ch)
coloredlogs.install(level="DEBUG", logger=logger)


def calcmd5(filename):
    chunksize = 2**29  # 512 MiB
    md5hash = hashlib.md5()
    with open(filename, mode="rb") as f:
        readchunk = partial(f.read, chunksize)
        for chunk in iter(readchunk, b""):
            md5hash.update(chunk)
    return md5hash.hexdigest()


def getLocation(filepath):
    parts = Path(filepath).parent.parts
    for part in reversed(parts):
        res = __match_location(part)
        if res:
            break
    else:
        return None
    return res


def __match_location(part):
    # BMT
    pattern_bmt = r"BO ?(\d+)"
    m = match(pattern_bmt, part)
    if m:
        val = int(m[1])
        return f"BO {val:02}"

    # ORTHO
    pattern_bot1 = r"BOT ?(\d|$)"
    m = match(pattern_bot1, part)
    if m:
        val = m[1]
        val = int(val) if val else 0
        return f"O_BO{val}"

    pattern_bot2 = r"Ortho ?(BO|SAS)(\d)"
    m = match(pattern_bot2, part)
    if m:
        if m[1] == "SAS":
            return f"O_SAS{m[2]}"
        else:
            return f"O_BO{m[2]}"

    # MATER
    pattern_mater = r"Mater ?BO(\d)"
    m = match(pattern_mater, part)
    if m:
        return f"M_BO{m[1]}"

    if "sspi" in part.lower():
        return "SSPI"

    if "dechoc" in part.lower() or "edf ect" in part.lower():
        return "DECHOC"

    if "ortho" in part.lower():
        return "BOT0"

    # NRI
    pattern_nri = r".*(R[56]|R0[56])"
    m = match(pattern_nri, part)
    if m:
        return f"R0{m[1][-1]}"

    # ENDOSCOPIE
    if "endoscopie" in part.lower():
        return "ENDOS"

    return None
    return None

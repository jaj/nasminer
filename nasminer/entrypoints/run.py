import logging
from datetime import datetime
from pathlib import Path
from stat import filemode

from nasminer.common import config, logger
from nasminer.entrypoints.delete_old import main as delete_old
from nasminer.entrypoints.dumpdb import main as dumpdb
from nasminer.entrypoints.mine import main as mine
from nasminer.entrypoints.norm_syringes import main as norm_syringes


def log_debug():
    logger.debug("Running with the following configuration:")
    for k, v in config.items():
        logger.debug(f"{k}: {v}")
    logger.debug("---")
    datadir = Path(config["datadir"])
    logger.debug(f"datadir exists? {datadir.exists()}")
    logger.debug(f"datadir filemode: {filemode(datadir.stat().st_mode)}")
    outdir = Path(config["outputdir"])
    logger.debug(f"outputdir exists? {outdir.exists()}")
    logger.debug(f"outputdir filemode: {filemode(outdir.stat().st_mode)}")
    logger.debug("---")


def main():
    if bool(config["debug"]):
        logger.setLevel(logging.DEBUG)

    log_debug()

    logger.info(f"Running syringe normalization at {datetime.now()}.")
    norm_syringes()
    logger.info(f"Deleting obsolete database records at {datetime.now()}.")
    delete_old()
    logger.info(f"Indexing new files at {datetime.now()}.")
    mine()
    logger.info(f"Dumping database data at {datetime.now()}.")
    dumpdb()


if __name__ == "__main__":
    main()

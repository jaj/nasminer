import io
import os
from collections import defaultdict
from functools import partial
from os import path, walk
from zipfile import ZipFile

import numpy as np
import pandas as pd
from tqdm import tqdm

from nasminer.common import config, logger
from nasminer.syringes.match_syr_lbl import detect_known_label

pdtonum = partial(pd.to_numeric, errors="coerce")
readcsv = partial(pd.read_csv, sep=",", index_col=False)
dtformat = "%Y-%m-%d %H:%M:%S.%f"


def test_file(filepath):
    headers1 = b"datetime,syringe,volume"
    headers2 = b"timens,syringe,volume"
    headers3 = b"timestamp,syringe,volume"
    headers4 = b"timestampns,syringe,volume"
    with open(filepath, "rb") as f:
        line = f.readline().rstrip()
    if path.basename(filepath) == "seringues.txt":
        return "evenio"
    elif line == headers1:
        return "datetime"
    elif line == headers2:
        return "timens"
    elif line == headers3:
        return "timestamp"
    elif line == headers4:
        return "timestampns"
    else:
        return None


def transform_index(dttype, idx):
    if dttype == "datetime":
        idx = pd.to_datetime(idx, format=dtformat)
    elif dttype in ["timens", "timestamp", "timestampns"]:
        idx = pdtonum(idx)
        idx = pd.to_datetime(idx, unit="ns")
    else:
        idx = None
    return idx


def populate_dict(topdir):
    syrfolders = defaultdict(list)
    for root, _, files in tqdm(walk(topdir)):
        for filename in files:
            _, ext = path.splitext(filename)
            if ext.lower() not in [".csv", ".txt"]:
                continue
            filepath = path.join(root, filename)
            filetype = test_file(filepath)
            if filetype:
                logger.debug(f"Found {filename} of type {filetype} in {root}")
                syrfolders[root].append((filetype, filename))
    return syrfolders


def process_syre_file(filepath, dtfield):
    df = readcsv(filepath)
    idx = transform_index(dtfield, df[dtfield])
    df["timestamp"] = idx
    df = df.pivot_table(index="timestamp", columns="syringe", values="volume")
    return df


def process_evenio_file(filepath):
    with open(filepath, "rb") as ftmp:
        line1 = ftmp.readline()
        ftmp.seek(0)
        fd = io.BytesIO()
        if not line1.startswith(b"temps,"):
            # One version of Evenio forgets the time header. Prepend it.
            fd.write(b"temps,")
        fd.write(ftmp.read())
    fd.seek(0)
    df = readcsv(fd, encoding="latin1")
    idx = transform_index("datetime", df["temps"])
    df["timestamp"] = idx
    df = df.set_index("timestamp")
    df = df.apply(pdtonum)
    return df


def merge_files(root, files):
    dfs = []
    for filetype, filename in files:
        filepath = path.join(root, filename)
        if filetype == "evenio":
            dftmp = process_evenio_file(filepath)
        else:
            dftmp = process_syre_file(filepath, filetype)
        dfs.append(dftmp)
    df = pd.concat(dfs, axis=0)
    df = df.dropna(axis="columns", how="all")
    df = df.dropna(axis="rows", how="all")
    df = df.sort_index()
    df["timens"] = df.index.view(np.int64)
    return df


def transform_files(indict):
    logger.info(f"Found {len(indict)} new files to transform")
    for root, files in indict.items():
        logger.debug(f"Merging {root}")
        os.chdir(root)
        try:
            df = merge_files(root, files)
        except Exception:
            logger.exception("Exception in merge_files")
            continue
        df = df.rename(detect_known_label, axis=1)
        logger.debug(df.head())
        df.to_csv("seringues_norm.csv", index_label="datetime", date_format=dtformat)


def ziparchive_old(indict):
    for root, files in indict.items():
        logger.debug(f"Archiving {root}")
        os.chdir(root)
        with ZipFile("seringues_old.zip", "w") as myzip:
            for _, f in files:
                myzip.write(f)


def rm_old(indict):
    for root, files in indict.items():
        logger.debug(f"Cleaning {root}")
        os.chdir(root)
        for _, f in files:
            os.remove(f)


def main():
    topdir = config["datadir"]
    if topdir is None:
        logger.error("Please set NASMINER_DATADIR")
        exit(1)
    logger.info("norm_syringes: scan files.")
    d = populate_dict(topdir)
    logger.info("norm_syringes: normalize files.")
    transform_files(d)
    logger.info("norm_syringes: archive old files.")
    ziparchive_old(d)
    rm_old(d)


if __name__ == "__main__":
    main()

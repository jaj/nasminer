import time

from nasminer import crawler
from nasminer.common import config, logger


def main():
    topdir = config["datadir"]
    if topdir is None:
        logger.error("Please set NASMINER_DATADIR")
        exit(1)

    start_time = time.time()
    crawler.run(topdir)
    end_time = time.time()
    logger.info(f"Ran for {end_time - start_time} seconds")


if __name__ == "__main__":
    main()

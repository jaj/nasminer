from os import path

import pandas as pd
from sqlalchemy import select

from nasminer import dicom, edf, ixtrend, mict, sql, syringes
from nasminer.common import config, logger

available_modules = [syringes, edf, ixtrend, dicom, mict]


tablename = f"{sql.schema}.files"
filest = sql.tables[tablename]


def dump_table(conn, tablename, outdir):
    modt = sql.tables[tablename]
    q = (
        select(modt, filest)
        .select_from(modt.join(filest, modt.c.fileid == filest.c.id))
        .order_by(filest.c.mtime)
    )
    df = pd.read_sql(q, conn)
    outfile = path.join(outdir, f"{tablename}.xlsx")
    logger.info(f"Dumping table {tablename} to {outfile}")
    df.to_excel(outfile, index=False)


def main():
    outdir = config["outputdir"]
    if outdir is None:
        logger.error("Please set NASMINER_OUTPUTDIR")
        exit(1)

    tablenames = [m.tablename for m in available_modules]

    with sql.db.connect() as conn:
        for t in tablenames:
            dump_table(conn, t, outdir)


if __name__ == "__main__":
    main()

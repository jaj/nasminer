from os import path, walk

import pandas as pd
from sqlalchemy.sql import and_
from tqdm import tqdm

from nasminer import sql
from nasminer.common import config, logger

tablename = "files"
tablename_with_schema = f"{sql.schema}.{tablename}"
filest = sql.tables[tablename_with_schema]


def read_existing(conn):
    df = pd.read_sql_table(
        tablename,
        conn,
        schema=sql.schema,
        columns=["mtime"],
        index_col=["folder", "filename"],
        parse_dates=["mtime"],
    )
    return df


def get_missing(topdir):
    with sql.db.connect() as conn:
        indb = read_existing(conn)
    logger.info(f"Entries in DB: {len(indb)}")

    ondisk = []
    for root, _, files in tqdm(walk(topdir, followlinks=True)):
        folder = path.relpath(root, topdir)
        for filename in files:
            ondisk.append((folder, filename))

    logger.info(f"Files on disk: {len(ondisk)}")
    idxondisk = pd.Index(ondisk, name=("folder", "filename"))
    missing = indb.index.difference(idxondisk)
    return missing


def run(topdir, n_max_delete_rows):
    missing = get_missing(topdir)
    if len(missing) > n_max_delete_rows:
        logger.error(
            f"Number of missing DB records exceeds NASMINER_DELETEMAXROWS ({len(missing)} > {n_max_delete_rows})"
        )
        return
    with sql.db.connect() as conn:
        for folder, filename in missing:
            d = filest.delete().where(
                and_(filest.c.folder == folder, filest.c.filename == filename)
            )
            conn.execute(d)
            conn.commit()


def main():
    try:
        maxdelete = int(config["deletemaxrows"])
    except ValueError:
        maxdelete = None

    topdir = config["datadir"]
    if topdir is None or maxdelete is None:
        logger.error("Please set NASMINER_DATADIR and NASMINER_DELETEMAXROWS")
        exit(1)
    run(topdir, n_max_delete_rows=maxdelete)


if __name__ == "__main__":
    main()

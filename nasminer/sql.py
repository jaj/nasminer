from sqlalchemy import MetaData, create_engine

from nasminer.common import config, logger

dburi = config["dburi"]
if dburi is None:
    logger.error("Please set NASMINER_DBURI")
    exit(1)

if config["debug"]:
    db = create_engine(dburi, echo=True)
else:
    db = create_engine(dburi)

schema = config["dbschema"]
meta = MetaData(schema=schema)
meta.reflect(bind=db)
tables = meta.tables

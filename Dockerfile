FROM python:3.12-alpine

WORKDIR /app

RUN pip install --no-cache-dir -U pip

# Install psycopg2 with C dependencies
RUN apk add --no-cache postgresql-client
RUN apk add --no-cache --virtual .build-deps \
    gcc postgresql-dev python3-dev musl-dev
RUN pip install --no-cache-dir psycopg2

RUN pip install --no-cache-dir "flit_core >=2,<4"

COPY . .
RUN pip install --no-cache-dir .

RUN apk del .build-deps

ENTRYPOINT [ "nasminer_run" ]
#ENTRYPOINT [ "crond", "-f" ]
#/etc/crontabs/root
